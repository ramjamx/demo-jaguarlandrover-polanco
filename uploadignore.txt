bower_components
node_modules
npm-debug.log
yarn-error.log
vendor
.DS_Store
rsync.log
config\.js
.eslintrc
.git*
.travis.yml
config.example.js
package.json
*.md
webpack*
yarn.lock
uploadignore.txt
subirCambios
cockpit/*
