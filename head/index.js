/**
 * Head de vue-meta
 * @type {Object}
 * @url https://github.com/declandewet/vue-meta
 */
const title = 'Jaguar Land Rover Polanco'
const description = 'Jaguar Land Rover Polanco'
const url = 'jlr-polanco.dinamo.mx'

const icons = require('./icons')({ title })
const og = require('./og-data')({ title, url, description })
const schema = require('./schema')({ title, url, description })

/**
 * Propiedades meta de html
 * @type {Array} meta
 */
const meta = [
  { charset: 'utf-8' },
  // eslint-disable-next-line
  { name: 'keywords', content: 'jaguar, land rover, automotriz, demo' },
  { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1' },
  { hid: 'description', name: 'description', content: description },
  { name: 'google-site-verification', content: '' },
  { name: 'apple-mobile-web-app-capable', content: 'yes' },
  { name: 'apple-mobile-web-app-title', content: 'Jaguar Land Rover Polanco' },
]

const link = [
  {
    href: 'https://fonts.googleapis.com/css?family=Quicksand|Rubik:400,700',
    rel: 'stylesheet',
  },
  {
    href: '/assets/background.jpg',
    rel: 'apple-touch-startup-image',
  },
  {
    href: '/icons/apple-icon-180x180.png',
    rel: 'apple-touch-icon',
    sizes: '180x180',
  },
  {
    href: '/icons/apple-icon-152x152.png',
    rel: 'apple-touch-icon',
    sizes: '152x152',
  },
  {
    href: '/icons/apple-icon-144x144.png',
    rel: 'apple-touch-icon',
    sizes: '144x144',
  },
  {
    href: '/icons/apple-icon-120x120.png',
    rel: 'apple-touch-icon',
    sizes: '120x120',
  },
  {
    href: '/icons/apple-icon-114x114.png',
    rel: 'apple-touch-icon',
    sizes: '114x114',
  },
  {
    href: '/icons/apple-icon-76x76.png',
    rel: 'apple-touch-icon',
    sizes: '76x76',
  },
  {
    href: '/icons/apple-icon-72x72.png',
    rel: 'apple-touch-icon',
    sizes: '72x72',
  },
  {
    href: '/icons/apple-icon-60x60.png',
    rel: 'apple-touch-icon',
    sizes: '60x60',
  },
  {
    href: '/icons/apple-icon-57x57.png',
    rel: 'apple-touch-icon',
    sizes: '57x57',
  },
]


meta.splice(2, 0, ...og)
meta.splice(2, 0, ...icons.meta)
link.splice(2, 0, ...icons.link)

const head = {
  // If undefined or blank then we don't need the hyphen
  titleTemplate: titleChunk => (titleChunk ? `${titleChunk} - J LR` : 'J LR'),
  link,
  meta,
  script: [
    { innerHTML: JSON.stringify(schema), type: 'application/ld+json' },
  ],
  noscript: [
    { innerHTML: 'Para tener una experiencia óptima, habilita JavaScript en tu navegador.' },
  ],
  __dangerouslyDisableSanitizers: ['script'],
}

module.exports = head
